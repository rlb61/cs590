###########
# Imports #---------------------------------------------------------------------
###########
import io
import os
import random
import numpy as np
import pandas as pd
from gensim.models.keyedvectors import KeyedVectors
import matplotlib
from matplotlib import pylab
matplotlib.use('agg') #so that it does not attempt to display via SSH
import matplotlib.pyplot as plt
plt.ioff() #turn interactive plotting off
from utils import clean
from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dense, GRU, RepeatVector
###########################
# Globals/Hyperparameters #----------------------------------------------------
###########################
#Word vectors are saved in word2vec binary format
ENG_WORD_VECS_PATH = '/data/rlb61/classic_ml/GoogleNews-vectors-negative300.bin'
ENG_W2V_MODEL = KeyedVectors.load_word2vec_format(ENG_WORD_VECS_PATH, binary=True)
IND_WORD_VECS_PATH = '/data/rlb61/privacy/seq2seq-custom/Indonesian_Word2Vec/wiki.id.vec'
IND_FT_MODEL =  KeyedVectors.load_word2vec_format(IND_WORD_VECS_PATH)
EMBEDDING_DIM = 300 #embedding dimension
MAX_TIMESTEPS = 20 #max timesteps (max number of words in a sentence)
NUM_EPOCHS = 10
BATCH_SIZE = 128
#For tracking missing words percent:
WORDS_TOTAL = 0
MISSING_WORDS = 0
#############
# Functions #-------------------------------------------------------------------
#############
def prepare_corpus(language, corpus_path):
    """'language' is either 'english' or 'indonesian'
    Returned data will have shape [num_examples, timesteps, embedding_dimension]
    where for a sentence <timesteps> = MAX_TIMESTEPS = the maximum number
    of words in a sentence.
    """
    x = []
    not_decodable = 0
    with open(corpus_path,'rb') as f:
        for line in f:
            try:
                line = line.decode('utf-8',errors='ignore')
            except:
                line = ''
                not_decodable += 1
            phrase = clean(line.strip())
            x.append(buildWordVector(phrase.split(),language))
    track_missing_words()
    print('\tNot Decodable:',not_decodable)
    return np.array(x)
def track_missing_words():
    """Report how many words are missing from the word2vec pretrained vectors"""
    global WORDS_TOTAL, MISSING_WORDS
    print('\tWORDS_TOTAL:',WORDS_TOTAL,'\n\tMISSING_WORDS:',MISSING_WORDS,
          '\n\tFrac Missing Words:',MISSING_WORDS/WORDS_TOTAL)
    WORDS_TOTAL = 0; MISSING_WORDS = 0
def buildWordVector(tokens, language):
    """<tokens> is a list of strings. This function returns a numpy array
    of dimension MAX_TIMESTEPS x EMBEDDING_DIM. Any words that occur
    beyond MAX_TIMESTEPS are simply not included.
    
    <w2v_model> is loaded from gensim
    """
    global WORDS_TOTAL, MISSING_WORDS
    vec = np.zeros((MAX_TIMESTEPS, EMBEDDING_DIM))
    max_len = min(MAX_TIMESTEPS, len(tokens))
    for index in range(max_len):
        WORDS_TOTAL+=1
        word = tokens[index]
        try:
            if language=='english':
                vec[index,:] = ENG_W2V_MODEL[word]
            elif language=='indonesian':
                vec[index,:] = IND_FT_MODEL[word]
            else:
                assert False, 'language must be english or indonesian'
        except KeyError: # handling the case where the token is not in the corpus
            MISSING_WORDS+=1
            continue
    return vec
def write_out_pred_vecs_as_english(key, pred_vecs):
    """Given predicted output of shape [num_examples, timesteps, embedding_dimension]
    transform the predicted word vectors into the closest English word
    for downstream evaluation"""
    translation_error = 0
    with open(key+'_predicted_sentences.txt','w') as f:
        for exampleidx in range(pred_vecs.shape[0]):
            sentence_array = pred_vecs[exampleidx,:,:]
            sentence = ''
            for word_idx in range(sentence_array.shape[0]):
                word_vec = sentence_array[word_idx,:]
                #Note: you have to supply the word vector inside of a list
                #otherwise gensim tries to iterate through the elements of the
                #word vector and throws an error
                english_word = ENG_W2V_MODEL.most_similar(positive=[word_vec], topn=1)
                #english_word is a list of tuples: [('Someword', 0.6700047850608826)]
                sentence += english_word[0][0] #extract just the word part
            try:
                sentence = (sentence.encode('ascii', errors ='ignore')).decode('utf-8')
                f.write(sentence+'\n')
            except:
                f.write('translation-error\n')
                translation_error+=1
    print('\tFor',key,'there were',translation_error,'untranslatable sentences out of',pred_vecs.shape[0],'sentences total')
def plot_history(history):
    #Plots
    #https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # list all data in history
    print(history.history.keys())
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Loss_During_Training.pdf')
    plt.close()
#######
# Run #-------------------------------------------------------------------------
#######
if __name__ == '__main__':
    # """Translation from Indonesian to English"""
    print('Now preparing the Indonesian SMERU corpus')
    indonesian_corpus = prepare_corpus('indonesian', '/data/rlb61/privacy/seq2seq-custom/Parallel_IndEng_Corpora/SMERU-26870.id')
    print('Now preparing the English SMERU corpus')
    english_corpus = prepare_corpus('english', '/data/rlb61/privacy/seq2seq-custom/Parallel_IndEng_Corpora/SMERU-26870.en')
    
    #Split into 80% train and 20% test
    train_index = int(0.8*(english_corpus.shape[0]))
    train_english = english_corpus[0:train_index,:,:]
    test_english = english_corpus[train_index:,:,:]
    train_indo = indonesian_corpus[0:train_index,:,:]
    test_indo = indonesian_corpus[train_index:,:,:]
    
    print('Now preparing the job sentences')
    jobs_path = '/data/rlb61/privacy/seq2seq-custom/jobs_lang_data/categorized_jobs_ind/'
    jobs_indonesian = {
        'artistic':prepare_corpus('indonesian',os.path.join(jobs_path,'artistic-jobs-ind.txt')),
        'dance':prepare_corpus('indonesian',os.path.join(jobs_path,'dance-jobs-ind.txt')),
        'industrial':prepare_corpus('indonesian',os.path.join(jobs_path,'industrial-jobs-ind.txt')),
        'theatre':prepare_corpus('indonesian',os.path.join(jobs_path,'theatre-jobs-ind.txt')),
        'computer':prepare_corpus('indonesian',os.path.join(jobs_path,'computer-jobs-ind.txt')),
        'film-television':prepare_corpus('indonesian',os.path.join(jobs_path,'film-television-jobs-ind.txt')),
        'science':prepare_corpus('indonesian',os.path.join(jobs_path,'science-jobs-ind.txt')),
        'writing':prepare_corpus('indonesian',os.path.join(jobs_path,'writing-jobs-ind.txt')),
        'corporate':prepare_corpus('indonesian',os.path.join(jobs_path,'corporate-jobs-ind.txt')),
        'healthcare':prepare_corpus('indonesian',os.path.join(jobs_path,'healthcare-jobs-ind.txt')),
        'service':prepare_corpus('indonesian',os.path.join(jobs_path,'service-jobs-ind.txt'))}
    
    #References
    #https://machinelearningmastery.com/lstm-autoencoders/
    #https://machinelearningmastery.com/encoder-decoder-long-short-term-memory-networks/
    #https://stackoverflow.com/questions/47268608/confusion-about-keras-rnn-input-shape-requirement
    #https://machinelearningmastery.com/define-encoder-decoder-sequence-sequence-model-neural-machine-translation-keras/
    #Encoder/Decoder architecture: https://machinelearningmastery.com/lstm-autoencoders/
    #Encoder/Decoder architecture II: https://machinelearningmastery.com/encoder-decoder-long-short-term-memory-networks/
    #Input shape info: https://stackoverflow.com/questions/47268608/confusion-about-keras-rnn-input-shape-requirement
    #https://github.com/keras-team/keras/issues/562
    #Temp:
    # train_indo = np.zeros((100,MAX_TIMESTEPS,EMBEDDING_DIM))
    # train_english = np.ones((100,MAX_TIMESTEPS,EMBEDDING_DIM))
    # jobs_indonesian = {'fake':train_indo}
    # test_indo = train_indo
    # test_english = train_english
    
    model = Sequential()
    model.add(LSTM(units=300,input_shape=(MAX_TIMESTEPS,EMBEDDING_DIM)))
    model.add(RepeatVector(MAX_TIMESTEPS))
    model.add(LSTM(300,return_sequences=True))
    model.summary()
    model.compile(loss='mse', optimizer='rmsprop', metrics=['accuracy'])
    history = model.fit(train_indo, train_english,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=[test_indo, test_english])
    plot_history(history)
    
    #Results on custom dataset
    for key in jobs_indonesian:
        pred_vecs = model.predict(jobs_indonesian[key])
        write_out_pred_vecs_as_english(key, pred_vecs)
    
    #Save model
    model.save('trained_model.h5')
