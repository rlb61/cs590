import sys

file_in = open('../lang_data/original_occupation_list.txt', 'r')
file_out = open('../lang_data/clean_occupation_list.txt', 'w')
sys.stdout = file_out

while True:
    # Only process lines that start with an id & id does not end with a 0
    line = file_in.readline()
    # check if we've reached end of file
    if not line:
        break
    # if not, then parse the current line to get occupations
    line_list = line.split()
    first_string = line_list[0]
    
    # id of occupations dont end with 0
    if first_string.isdigit() and first_string[len(first_string) - 1] != '0':
        occupation = ""
        for i in range(1, len(line_list)):
            if len(line_list[i]) > 1 and not line_list[i].isdigit():
                occupation = occupation + " " + line_list[i]
        print(occupation)

# close files
file_in.close()
file_out.close()
