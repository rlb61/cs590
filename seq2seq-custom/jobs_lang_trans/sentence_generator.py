import sys

file_in = open('../lang_data/clean_ind_occupation_list.txt', 'r')
file_out = open('../lang_data/ind_sentences.txt', 'w')
sys.stdout = file_out

while True:
    # Only process lines that start with an id & id does not end with a 0
    line = file_in.readline().rstrip()
    # check if we've reached end of file
    if not line:
        break
    # if not, then parse the current line to print
    sentence  = "dia adalah seorang " + line.lower()
    print(sentence)
    

# close files
file_in.close()
file_out.close()
