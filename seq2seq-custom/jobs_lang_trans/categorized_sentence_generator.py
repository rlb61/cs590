import sys
import os

directory = '../lang_data/categorized_jobs_ind'
for filename in os.listdir(directory):
    filepath = directory + '/' + filename
    file_in = open(filepath, 'r')
    filename_list = filename.split('-')
    output_name = filename_list[0] + '-' + filename_list[1] + "-sentences" + '.txt'
    output_path = '../lang_data/categorized_sentences_ind/' + output_name
    file_out = open(output_path, 'w')
    sys.stdout = file_out

    while True:
        # Only process lines that start with an id & id does not end with a 0
        line = file_in.readline().rstrip()
        # check if we've reached end of file
        if not line:
            break
        # if not, then parse the current line to print
        sentence = "dia adalah seorang " + line.lower()
        print(sentence)

    # close files
    file_in.close()
    file_out.close()
