###########
# Imports #---------------------------------------------------------------------
###########
import io
import os
import random
import numpy as np
import pandas as pd
from gensim.models.keyedvectors import KeyedVectors
import matplotlib
from matplotlib import pylab
matplotlib.use('agg') #so that it does not attempt to display via SSH
import matplotlib.pyplot as plt
plt.ioff() #turn interactive plotting off
from utils import clean
from keras.models import Model, Sequential
from keras.layers import Input, LSTM, Dense, GRU, RepeatVector
###########################
# Globals/Hyperparameters #----------------------------------------------------
###########################
#Word vectors are saved in word2vec binary format
ENG_WORD_VECS_PATH = '/data/rlb61/classic_ml/GoogleNews-vectors-negative300.bin'
ENG_W2V_MODEL = KeyedVectors.load_word2vec_format(ENG_WORD_VECS_PATH, binary=True)
IND_WORD_VECS_PATH = '/data/rlb61/privacy/seq2seq-custom/Indonesian_Word2Vec/wiki.id.vec'
IND_FT_MODEL =  KeyedVectors.load_word2vec_format(IND_WORD_VECS_PATH)
EMBEDDING_DIM = 300 #embedding dimension
MAX_TIMESTEPS = 6 #max timesteps (max number of words in a sentence)
NUM_EPOCHS = 30
BATCH_SIZE = 128
#For tracking missing words percent:
WORDS_TOTAL = 0
MISSING_WORDS = 0
#############
# Functions #-------------------------------------------------------------------
#############
def prepare_corpus(language, corpus_path):
    """'language' is either 'english' or 'indonesian'
    Returned data will have shape [num_examples, timesteps, embedding_dimension]
    where for a sentence <timesteps> = MAX_TIMESTEPS = the maximum number
    of words in a sentence.
    """
    x = []
    other = []
    sentences = []
    not_decodable = 0
    with open(corpus_path,'rb') as f:
        for line in f:
            try:
                line = line.decode('utf-8',errors='ignore')
            except:
                line = ''
                not_decodable += 1
            phrase = clean(line.strip())
            if language == 'indonesian':
                #append twice since he and she are the same
                phrase_as_vectors = buildWordVector(phrase.split(),language)
                x.append(phrase_as_vectors)
                x.append(phrase_as_vectors)
                other.append(phrase_as_vectors) #append only once
                sentences.append(phrase)
            if language == 'english':
                sentences.append(phrase)
                #phrase is just a job, usually pluralized
                #remove the ies or s so it's singular
                if phrase[-3:]=='ies':
                    phrase = phrase.replace('ies','y')
                elif phrase[-1] == 's':
                    phrase = phrase[0:-1]
                x.append(buildWordVector(['He','is','a',phrase],language))
                x.append(buildWordVector(['She','is','a',phrase],language))
    track_missing_words()
    print('\tNot Decodable:',not_decodable)
    if language=='indonesian':
        return np.array(x), np.array(other), sentences
    elif language=='english':
        return np.array(x), sentences
def track_missing_words():
    """Report how many words are missing from the word2vec pretrained vectors"""
    global WORDS_TOTAL, MISSING_WORDS
    print('\tWORDS_TOTAL:',WORDS_TOTAL,'\n\tMISSING_WORDS:',MISSING_WORDS,
          '\n\tFrac Missing Words:',MISSING_WORDS/WORDS_TOTAL)
    WORDS_TOTAL = 0; MISSING_WORDS = 0
def buildWordVector(tokens, language):
    """<tokens> is a list of strings. This function returns a numpy array
    of dimension MAX_TIMESTEPS x EMBEDDING_DIM. Any words that occur
    beyond MAX_TIMESTEPS are simply not included.
    
    <w2v_model> is loaded from gensim
    """
    global WORDS_TOTAL, MISSING_WORDS
    vec = np.zeros((MAX_TIMESTEPS, EMBEDDING_DIM))
    max_len = min(MAX_TIMESTEPS, len(tokens))
    for index in range(max_len):
        WORDS_TOTAL+=1
        word = tokens[index]
        try:
            if language=='english':
                vec[index,:] = ENG_W2V_MODEL[word]
            elif language=='indonesian':
                vec[index,:] = IND_FT_MODEL[word]
            else:
                assert False, 'language must be english or indonesian'
        except KeyError: # handling the case where the token is not in the corpus
            MISSING_WORDS+=1
            continue
    return vec
def transform_pred_vecs_to_english(pred_vecs):
    """Given predicted output of shape [num_examples, timesteps, embedding_dimension]
    transform the predicted word vectors into the closest English word
    for downstream evaluation, and return a list"""
    translation_error = 0
    output = []
    for exampleidx in range(pred_vecs.shape[0]):
        sentence_array = pred_vecs[exampleidx,:,:]
        sentence = ''
        for word_idx in range(sentence_array.shape[0]):
            word_vec = sentence_array[word_idx,:]
            #Note: you have to supply the word vector inside of a list
            #otherwise gensim tries to iterate through the elements of the
            #word vector and throws an error
            english_word = ENG_W2V_MODEL.most_similar(positive=[word_vec], topn=1)
            #english_word is a list of tuples: [('Someword', 0.6700047850608826)]
            if english_word[0][1] < 0.1: #too far away
                sentence += ' notfound'
            else:
                sentence += ' '+english_word[0][0] #extract just the word part
        try:
            sentence = (sentence.encode('ascii', errors ='ignore')).decode('utf-8')
            output.append(sentence)
        except:
            output.append('translation-error')
            translation_error+=1
    print('There were',translation_error,'untranslatable sentences out of',pred_vecs.shape[0],'sentences total')
    return output
def plot_history(history):
    #Plots
    #https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # list all data in history
    print(history.history.keys())
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Loss_During_Training.pdf')
    plt.close()
#######
# Run #-------------------------------------------------------------------------
#######
if __name__ == '__main__':
    #Translation from Indonesian to English
    print('Now preparing the Indonesian sentences')
    indonesian_corpus, test_corpus, test_sentences = prepare_corpus('indonesian', '/data/rlb61/privacy/seq2seq-custom/jobs_lang_data/ind_sentences.txt')
    
    print('Now preparing the English sentences')
    english_corpus, english_sentences = prepare_corpus('english', '/data/rlb61/privacy/seq2seq-custom/jobs_lang_data/clean_occupation_list.txt')
    
    #Shuffle
    idx = np.arange(0, indonesian_corpus.shape[0])
    seed = np.random.randint(0,10e6)
    np.random.seed(seed)
    print('Shuffling based on seed',str(seed))
    np.random.shuffle(idx)
    indonesian_corpus = indonesian_corpus[idx,:,:]
    english_corpus = english_corpus[idx,:,:]
    
    #Split into 90% train and 10% test
    train_index = int(0.9*(english_corpus.shape[0]))
    train_english = english_corpus[0:train_index,:,:]
    test_english = english_corpus[train_index:,:,:]
    train_indo = indonesian_corpus[0:train_index,:,:]
    test_indo = indonesian_corpus[train_index:,:,:]
    
    #References
    #https://machinelearningmastery.com/lstm-autoencoders/
    #https://machinelearningmastery.com/encoder-decoder-long-short-term-memory-networks/
    #https://stackoverflow.com/questions/47268608/confusion-about-keras-rnn-input-shape-requirement
    #https://machinelearningmastery.com/define-encoder-decoder-sequence-sequence-model-neural-machine-translation-keras/
    #Encoder/Decoder architecture: https://machinelearningmastery.com/lstm-autoencoders/
    #Encoder/Decoder architecture II: https://machinelearningmastery.com/encoder-decoder-long-short-term-memory-networks/
    #Input shape info: https://stackoverflow.com/questions/47268608/confusion-about-keras-rnn-input-shape-requirement
    #https://github.com/keras-team/keras/issues/562
    #Temp:
    # train_indo = np.zeros((100,MAX_TIMESTEPS,EMBEDDING_DIM))
    # train_english = np.ones((100,MAX_TIMESTEPS,EMBEDDING_DIM))
    # jobs_indonesian = {'fake':train_indo}
    # test_indo = train_indo
    # test_english = train_english
    
    model = Sequential()
    model.add(LSTM(units=300,input_shape=(MAX_TIMESTEPS,EMBEDDING_DIM)))
    model.add(RepeatVector(MAX_TIMESTEPS))
    model.add(LSTM(300,return_sequences=True))
    model.summary()
    model.compile(loss='mse', optimizer='rmsprop', metrics=['accuracy'])
    history = model.fit(train_indo, train_english,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=[test_indo, test_english])
    plot_history(history)
    
    #Results on custom dataset
    predictions = model.predict(test_corpus)
    predicted_sentences = transform_pred_vecs_to_english(predictions)
    true_out = pd.DataFrame(np.reshape([predicted_sentences,test_sentences,english_sentences],(len(predicted_sentences),3)),
        columns = ['Predicted_English_Sentence','Input_Indonesian_Sentence','True_English_Job'])
    true_out.to_csv('Model_Translation_Predictions_All_Jobs.csv')
    
    #Save model
    model.save('trained_model.h5')
