BASELINE (BIASED) ENGLISH WORD2VEC
Pretrained English Word2Vec (Google News):
https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit?usp=sharing

HATE SPEECH CLASSIFICATION
biLSTMs, hate speech classification:
https://github.com/sebastiandziadzio/hate-tweet
https://www.kaggle.com/snlpnkj/bidirectional-lstm-keras

LANGUAGE TRANSLATION
Categorized Job Titles:
https://github.com/marceloprates/Gender-Bias/tree/master/jobs
Corresponding to this paper: Prates et al. "Assessing Gender Bias in Machine Translation - A Case study with Google Translate." https://arxiv.org/abs/1809.02208

seq2seq original implementation:
https://github.com/farizrahman4u/seq2seq

recurrentshop dependency:
https://github.com/farizrahman4u/recurrentshop

English-Indonesian Parallel Corpora:
https://github.com/desmond86/Indonesian-English-Bilingual-Corpus

Pretrained Indonesian Word2Vec:
https://github.com/Kyubyong/wordvectors

DEBIASING ALGORITHMS
https://github.com/tolga-b/debiaswe
Corresponding to this paper: Bolukbasi et al. "Man is to Computer Programmer as Woman is to Homemaker? Debiasing Word Embeddings" NIPS 2016