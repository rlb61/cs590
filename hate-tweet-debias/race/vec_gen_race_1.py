###########
# Imports #---------------------------------------------------------------------
###########
# import debias.we as we
# from debias.we import WordEmbedding
import numpy as np
import simplejson as json
import we
from we import WordEmbedding

from gensim.models.keyedvectors import KeyedVectors

#####################
# Globals/Hyperparameters #----------------------------------------------------
###########################
#Word vectors are saved in word2vec binary format
# Download the file from https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit
WORD_VECS_PATH = '../../hate-tweet-data/debias_words/GoogleNews-vectors-negative300.bin'

# Debiasing global parameters
E = WordEmbedding(WORD_VECS_PATH)

"""
----------------Debiasing methods------------------------------------------------
"""
def hard_debias(E_ori, gender_specific_words, definitional, equalize):
    global E

    gender_direction = we.doPCA(definitional, E_ori,len(definitional)).components_[0]
    specific_set = set(gender_specific_words)
    for i, w in enumerate(E_ori.words):
        if w not in specific_set:
            E_ori.vecs[i] = we.drop(E_ori.vecs[i], gender_direction)
    E_ori.normalize()
    candidates = {x for e1, e2 in equalize for x in [(e1.lower(), e2.lower()),
                                                     (e1.title(), e2.title()),
                                                     (e1.upper(), e2.upper())]}
    print(candidates)
    for (a, b) in candidates:
        if (a in E_ori.index and b in E_ori.index):
            y = we.drop((E_ori.v(a) + E_ori.v(b)) / 2, gender_direction)
            z = np.sqrt(1 - np.linalg.norm(y)**2)
            if (E_ori.v(a) - E_ori.v(b)).dot(gender_direction) < 0:
                z = -z
            E_ori.vecs[E_ori.index[a]] = z * gender_direction + y
            E_ori.vecs[E_ori.index[b]] = -z * gender_direction + y
    E_ori.normalize()
    E = E_ori


if __name__ == '__main__':
    print("starting")
    # Load some gender related word lists for debiasing

    # get definitional pairs e.g (woman, man)
    with open('../../hate-tweet-data/debias_words/race/def_race_1.json', "r") as f:
        defs = json.load(f)
    print("definitional", defs)
    gender_specific_words = []
    equalize_pairs = []

    hard_debias(E, gender_specific_words, defs, equalize_pairs)

    E.save_w2v('../../hate-tweet-data/debias_words/race/GoogleNews-race-vectors-debiased-1.bin')
