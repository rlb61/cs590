###########
# Imports #---------------------------------------------------------------------
###########
import os
import random
import numpy as np
import pandas as pd
import keras.optimizers
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, GlobalMaxPool1D
from keras.models import Sequential
from keras.preprocessing import sequence
from keras.preprocessing.text import one_hot
from gensim.models.keyedvectors import KeyedVectors
import sklearn.metrics
import matplotlib
matplotlib.use('agg') #so that it does not attempt to display via SSH
import matplotlib.pyplot as plt
plt.ioff() #turn interactive plotting off
from utils import clean
###########################
# Globals/Hyperparameters #----------------------------------------------------
###########################
DATA_PATH = '../../hate-tweet-data/jigsaw_clean'
MANUAL_DATA = '../../hate-tweet-data/manual_labels/test_manual_big.csv'
#Word vectors are saved in word2vec binary format
WORD_VECS_PATH = '../../hate-tweet-data/debias_words/race/GoogleNews-race-vectors-debiased-2.bin'
W2V_MODEL = KeyedVectors.load_word2vec_format(WORD_VECS_PATH, binary=True) 
EMBEDDING_DIM = 300 #embedding dimension
MAX_TIMESTEPS = 60 #max timesteps (max number of words in a sentence)
NUM_EPOCHS = 6
BATCH_SIZE = 128
#For tracking missing words percent:
WORDS_TOTAL = 0
MISSING_WORDS = 0
#############
# Functions #-------------------------------------------------------------------
#############
def prepare_manual_data():
    """Return numpy array of dims [batch_size, timesteps, embedding_dimension]
    for the manually labeled test data"""
    df = (pd.read_csv(MANUAL_DATA)).iloc[0:998,:]
    x = []
    for dirtytweet in df['comment_text'].values:
        tweet = (clean(dirtytweet))
        x.append(buildWordVector(tweet.split()))
    track_missing_words()
    return np.array(x)
    
def read_data(filename):
    """Returned data will have shape [batch_size, timesteps, embedding_dimension]
    where for a sentence <timesteps> = MAX_TIMESTEPS = the maximum number
    of words in a sentence."""
    print('Preparing',filename)
    df = pd.read_csv(filename)
    x=[]
    for index in df.index.values:
        tweet = df.loc[index,'text']
        x.append(buildWordVector(tweet.split()))
    track_missing_words()
    y = (df['label'].values).astype('int')
    print('\n\tFrac Labels Toxic:',float(sum(y))/len(y))
    return (np.array(x), y)
def track_missing_words():
    """Report how many words are missing from the word2vec pretrained vectors"""
    global WORDS_TOTAL, MISSING_WORDS
    print('\tWORDS_TOTAL:',WORDS_TOTAL,'\n\tMISSING_WORDS:',MISSING_WORDS,
          '\n\tFrac Missing Words:',MISSING_WORDS/WORDS_TOTAL)
    WORDS_TOTAL = 0; MISSING_WORDS = 0
def buildWordVector(tokens):
    """<tokens> is a list of strings. This function returns a numpy array
    of dimension MAX_TIMESTEPS x EMBEDDING_DIM. Any words that occur
    beyond MAX_TIMESTEPS are simply not included."""
    global WORDS_TOTAL, MISSING_WORDS
    vec = np.zeros((MAX_TIMESTEPS, EMBEDDING_DIM))
    max_len = min(MAX_TIMESTEPS, len(tokens))
    for index in range(max_len):
        WORDS_TOTAL+=1
        word = tokens[index]
        try:
            vec[index,:] = W2V_MODEL[word]
        except KeyError: # handling the case where the token is not in the corpus
            MISSING_WORDS+=1
            continue
    return vec
def report_performance(pred_probs, true_labels):
    print('Performance:')
    fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_true = true_labels,
                                     y_score = pred_probs,
                                     pos_label = 1) #admitted is 1; not admitted is 0
    print('\tAUC:',sklearn.metrics.auc(fpr, tpr)) 
    print('\tAvg Precision:', sklearn.metrics.average_precision_score(true_labels, pred_probs))
    plot_roc_curve(fpr, tpr)
    plot_precision_recall_curve(true_labels, pred_probs)
def plot_history(history):
    #Plots
    #https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # list all data in history
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Accuracy_During_Training_Debiased_Race_2.pdf')
    plt.close()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Loss_During_Training_Debiased_Race_2.pdf')
    plt.close()
def plot_roc_curve(fpr, tpr):
    #http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py
    roc_auc = sklearn.metrics.auc(fpr, tpr)
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.savefig('Test_ROC_Curve_Debiased_Race_2.pdf')
    plt.close()
def plot_precision_recall_curve(true_labels, pred_probs):
    """<filename_prefix> e.g. MLP_Test"""
    #http://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html
    average_precision = sklearn.metrics.average_precision_score(true_labels, pred_probs)
    precision, recall, _ = sklearn.metrics.precision_recall_curve(true_labels, pred_probs)
    plt.step(recall, precision, color='b', alpha=0.2,
             where='post')
    plt.fill_between(recall, precision, step='post', alpha=0.2,
                     color='b')
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(
              average_precision))
    plt.savefig('Test_PR_Curve_Debiased_Race_2.pdf')
    plt.close()
#######
# Run #-------------------------------------------------------------------------
#######
if __name__ == '__main__':
    #see https://www.kaggle.com/snlpnkj/bidirectional-lstm-keras
    manual_data_array = prepare_manual_data()
    x_train, y_train = read_data(os.path.join(DATA_PATH,'train_clean.txt'))
    x_test, y_test = read_data(os.path.join(DATA_PATH, 'test_clean.txt'))
    
    model = Sequential()
    model.add(Bidirectional(LSTM(100, return_sequences=True, dropout=0.25,recurrent_dropout=0.1)))
    model.add(GlobalMaxPool1D())
    model.add(Dense(100, activation="relu"))
    model.add(Dropout(0.25))
    model.add(Dense(1, activation='sigmoid'))
    model.compile('adam', 'binary_crossentropy',
                  metrics=['accuracy'])
    history = model.fit(x_train, y_train,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=[x_test, y_test])
    plot_history(history)
    #Performance
    probabilities = model.predict(x_test)
    report_performance(pred_probs = probabilities, true_labels = y_test)
    
    #Results on custom dataset
    outdf = (pd.read_csv(MANUAL_DATA)).iloc[0:998,:]
    outdf['Predicted_Probabilities'] = model.predict(manual_data_array)
    outdf.to_csv('test_manual_big_with_pred_probs_debiased_race_2.csv')

