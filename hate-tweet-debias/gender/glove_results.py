import pandas as pd

DATA_PATH = '../../hate-tweet-data/jigsaw_clean'
MANUAL_DATA = '../../hate-tweet-data/manual_labels/test_manual_big.csv'
OUTPUT_DATA = 'test_manual_big_with_pred_glove_debias.csv'
true = (pd.read_csv(MANUAL_DATA)).iloc[0:998,:]
result = (pd.read_csv(OUTPUT_DATA)).iloc[0:998,:]
gender_toxic = true['gender']
gender_nontoxic = true['gender_nontoxic']
true_class = true['toxic']
pred_class = result['predicted']

# initialize counts for male
fp_m = 0
tp_m = 0
tn_m = 0
fn_m = 0
# initialize counts for female
fp_f = 0
tp_f = 0
tn_f = 0
fn_f = 0
#initialize counts for overall
fp = 0
tp = 0
tn = 0
fn = 0

for gender_toxic, gender_nontoxic, true, pred in zip(gender_toxic,
gender_nontoxic, true_class, pred_class):
    # false positive count
    if pred == 1 and true == 0:
        fp += 1
        if gender_nontoxic == 'f':
            fp_f += 1
        elif gender_nontoxic == 'm':
            fp_m += 1
    # true positive count
    if pred == 1 and true == 1:
        tp += 1
        if gender_toxic == 'f':
            tp_f += 1
        elif gender_toxic == 'm':
            tp_m += 1
    # true negative count
    if pred == 0 and true == 0:
        tn += 1
        if gender_nontoxic == 'f':
            tn_f += 1
        elif gender_nontoxic == 'm':
            tn_m += 1
    # false negative count
    if pred == 0 and true == 1:
        fp += 1
        if gender_toxic == 'f':
            fp_f += 1
        elif gender_toxic == 'm':
            fp_m += 1
# print the false positive rate
print('\tOverall False Positive Rate:', fp/len(true_class))
# print the accuracy
print('\tOverall Accuracy:', (tp + tn)/(tp + tn + fp + fn))
# print male false positive rate
print('\tMale False Positive Rate:', fp_m/len(true_class))
# print male accuracy
print('\tMale Accuracy:', (tp_m + tn_m)/(tp_m + tn_m + fp_m + fn_m))
# print female false positive rate
print('\tFemale False Positive Rate:', fp_f/len(true_class))
# print female accuracy
print('\tFemale Accuracy:', (tp_f + tn_f)/(tp_f + tn_f + fp_f + fn_f))
