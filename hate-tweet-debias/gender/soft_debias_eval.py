###########
# Imports #---------------------------------------------------------------------
###########
# import debias.we as we
# from debias.we import WordEmbedding
from numpy import linalg as LA
import numpy as np
import cvxpy as cp
import simplejson as json
import we
from we import WordEmbedding

import os
import random
import numpy as np
import pandas as pd
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional, GlobalMaxPool1D
from keras.models import Sequential
from keras.preprocessing import sequence
from keras.preprocessing.text import one_hot
from gensim.models.keyedvectors import KeyedVectors
import sklearn.metrics
import matplotlib
matplotlib.use('agg') #so that it does not attempt to display via SSH
import matplotlib.pyplot as plt
plt.ioff() #turn interactive plotting off
from utils import clean
###########################
# Globals/Hyperparameters #----------------------------------------------------
###########################
DATA_PATH = '../data/jigsaw_clean'
MANUAL_DATA = '../data/manual_labels/test_manual_big.csv'
#Word vectors are saved in word2vec binary format
WORD_VECS_PATH = '../data/GoogleNews-vectors-negative300.bin'
W2V_MODEL = KeyedVectors.load_word2vec_format(WORD_VECS_PATH, binary=True) 
EMBEDDING_DIM = 300 #embedding dimension
MAX_TIMESTEPS = 60 #max timesteps (max number of words in a sentence)
NUM_EPOCHS = 10
BATCH_SIZE = 128
#For tracking missing words percent:
WORDS_TOTAL = 0
MISSING_WORDS = 0

E = WordEmbedding(WORD_VECS_PATH)

#############
# Functions #-------------------------------------------------------------------
#############
def prepare_manual_data():
    """Return numpy array of dims [batch_size, timesteps, embedding_dimension]
    for the manually labeled test data"""
    df = (pd.read_csv(MANUAL_DATA)).iloc[0:998,:]
    x = []
    for dirtytweet in df['comment_text'].values:
        tweet = clean(dirtytweet)
        x.append(buildWordVector(tweet.split()))
    track_missing_words()
    return np.array(x)
    
def read_data(filename):
    """Returned data will have shape [batch_size, timesteps, embedding_dimension]
    where for a sentence <timesteps> = MAX_TIMESTEPS = the maximum number
    of words in a sentence."""
    
    print('Preparing',filename)
    x=[]; y=[]
    with open(filename, 'r') as f:
        next(f)
        for line in f:
            line = line.strip()
            tweet, label = line.split('\t')
            x.append(buildWordVector(tweet.split()))
            if label=='__label__clean':
                y.append(0)
            elif label=='__label__toxic':
                y.append(1)
            else:
                assert False,'Invalid label:'+label
    track_missing_words()
    print('\n\tFrac Labels Toxic:',float(sum(y))/len(y))
    print('\n\txArray:', np.array(x).head()) 
    return (np.array(x), np.array(y))

def track_missing_words():
    """Report how many words are missing from the word2vec pretrained vectors"""
    global WORDS_TOTAL, MISSING_WORDS
    print('\tWORDS_TOTAL:',WORDS_TOTAL,'\n\tMISSING_WORDS:',MISSING_WORDS,
          '\n\tFrac Missing Words:',MISSING_WORDS/WORDS_TOTAL)
    WORDS_TOTAL = 0; MISSING_WORDS = 0

def buildWordVector(tokens):
    """<tokens> is a list of strings. This function returns a numpy array
    of dimension MAX_TIMESTEPS x EMBEDDING_DIM. Any words that occur
    beyond MAX_TIMESTEPS are simply not included."""
    global WORDS_TOTAL, MISSING_WORDS
    vec = np.zeros((MAX_TIMESTEPS, EMBEDDING_DIM))
    max_len = min(MAX_TIMESTEPS, len(tokens))
    for index in range(max_len):
        WORDS_TOTAL+=1
        word = tokens[index]
        try:
            #vec[index,:] = W2V_MODEL[word]
            vec[index,:] = E[word]
        except KeyError: # handling the case where the token is not in the corpus
            MISSING_WORDS+=1
            continue
    return vec

def report_performance(pred_probs, true_labels):
    print('Performance:')
    fpr, tpr, thresholds = sklearn.metrics.roc_curve(y_true = true_labels,
                                     y_score = pred_probs,
                                     pos_label = 1) #admitted is 1; not admitted is 0
    print('\tAUC:',sklearn.metrics.auc(fpr, tpr)) 
    print('\tAvg Precision:', sklearn.metrics.average_precision_score(true_labels, pred_probs))
    plot_roc_curve(fpr, tpr)
    plot_precision_recall_curve(true_labels, pred_probs)
def plot_history(history):
    #Plots
    #https://machinelearningmastery.com/display-deep-learning-model-training-history-in-keras/
    # list all data in history
    print(history.history.keys())
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Accuracy_During_Training_Soft_Debias.pdf')
    plt.close()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.savefig('Loss_During_Training_Soft_Debias.pdf')
    plt.close()
def plot_roc_curve(fpr, tpr):
    #http://scikit-learn.org/stable/auto_examples/model_selection/plot_roc.html#sphx-glr-auto-examples-model-selection-plot-roc-py
    roc_auc = sklearn.metrics.auc(fpr, tpr)
    lw = 2
    plt.plot(fpr, tpr, color='darkorange',
             lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.savefig('Test_ROC_Curve_Soft_Debias.pdf')
    plt.close()
def plot_precision_recall_curve(true_labels, pred_probs):
    """<filename_prefix> e.g. MLP_Test"""
    #http://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html
    average_precision = sklearn.metrics.average_precision_score(true_labels, pred_probs)
    precision, recall, _ = sklearn.metrics.precision_recall_curve(true_labels, pred_probs)
    plt.step(recall, precision, color='b', alpha=0.2,
             where='post')
    plt.fill_between(recall, precision, step='post', alpha=0.2,
                     color='b')
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(
              average_precision))
    plt.savefig('Test_PR_Curve_Soft_Debias.pdf')
    plt.close()


"""
    ----------------soft debiasing------------------------------------------------
"""

def get_gender_subspace(E, defining_sets):
    # get gender subspace
    gender_direction = we.doPCA(defining_sets, E).components_[0]
    return gender_direction

def get_neutral_embedding(E, gender_specific_words):
    """ Get the matrix of the embedding vectors corresponding to 
        gender neutral words
    """
    N = []
    specific_set = set(gender_specific_words)
    for w in E.words:
        if w not in specific_set:
            N.append(E.v(w))
    N = np.array(N)
    return N.transpose()

"""
    Inputs: B is the gender subspace
            W is the matrix of all embedding vectors
            N is the matrix of the embedding vectors
            corresponding to the gender neutral words
    Output:
""" 
def soft_debias(B, W, N, tuning_param):
    """ 
        T is the desired debiasing transformation that seeks
        to preserve pairwise inner products between all the 
        word vectors while minimizing the projection of the gender
        neutral words onto the gender subspace
    """
    
    # want to find the debiasing transformation T of size dxd where d is the
    # length of the word embedding
    global E
    
    # Let X = T transpose * T
    d = len(N)
    X = cp.Variable((d, d), PSD=True)
    # solve the semi-definite programming problem
    expr_1 = (cp.norm((np.transpose(W)@X@W - np.transpose(W)@W), 'fro'))**2
    expr_2 = (cp.norm((np.transpose(N)@X@B), 'fro'))**2
    objective = cp.Minimize(expr_1 + tuning_param*expr_2)
    constraints = []
    prob = cp.Problem(objective, constraints)
    result = prob.solve(verbose = True)

    # get T by using Cholesky Decomposition
    X = result
    T = np.linalg.cholesky(X)

    # P = 
    # q = 
    # G = np.identity(d) * -1
    # h = np.zeros((d, d,))
    # A = 
    # b = 

    sol = solvers.qp(Q, p, G, h, A, b)

    # initialize an output matrix of size the same as the original word embedding W
    output = W 
    # normalize the ouput embedding to have unit length
    for i in range(len(W)):
        for j in range(len(W[0])):
            output[i][j] = (W[i][j]*T)/LA.norm((W[i][j]*T))

    E = output 


#######
# Run #-------------------------------------------------------------------------
#######
if __name__ == '__main__':
    # Load some gender related word lists for debiasing

    # get definitional pairs e.g (woman, man)
    with open('../data/debias_words/definitional_pairs.json', "r") as f:
        defs = json.load(f)
    # print("definitional", defs)

    # with open('../data/debias_words/equalize_pairs.json', "r") as f:
    #     equalize_pairs = json.load(f)
    # print("equalize", equalize_pairs)

    # print the first 10/218 gender specific words e.g. acress, aunt, ballerina
    with open('../data/debias_words/gender_specific_seed.json', "r") as f:
        gender_specific_words = json.load(f)
    print("gender specific", len(gender_specific_words), gender_specific_words[:10])

    tuning_param = 0.2
    B = get_gender_subspace(E, defs)
    N = get_neutral_embedding(E, gender_specific_words)
    debias_E = soft_debias(B, E.vecs.transpose(), N, tuning_param)


    # #see https://www.kaggle.com/snlpnkj/bidirectional-lstm-keras
    # manual_data_array = prepare_manual_data()
    x_train, y_train = read_data(os.path.join(DATA_PATH,'train_clean.txt'))
    x_test, y_test = read_data(os.path.join(DATA_PATH, 'test_clean.txt'))

    model = Sequential()
    model.add(Bidirectional(LSTM(100, return_sequences=True, dropout=0.25,recurrent_dropout=0.1)))
    model.add(GlobalMaxPool1D())
    model.add(Dense(100, activation="relu"))
    model.add(Dropout(0.25))
    model.add(Dense(1, activation='sigmoid'))
    model.compile('adam', 'binary_crossentropy',
                   metrics=['accuracy'])
    history = model.fit(x_train, y_train,
            batch_size=BATCH_SIZE,
            epochs=NUM_EPOCHS,
            validation_data=[x_test, y_test])
    plot_history(history)
    
    #Performance
    probabilities = model.predict(x_test)
    report_performance(pred_probs = probabilities, true_labels = y_test)
    
    #Results on custom dataset
    outdf = (pd.read_csv(MANUAL_DATA)).iloc[0:998,:]
    outdf['Predicted_Probabilities'] = model.predict(manual_data_array)
    outdf.to_csv('test_manual_big_with_pred_probs_soft_debias.csv')
