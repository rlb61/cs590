###########
# Imports #---------------------------------------------------------------------
###########
# import debias.we as we
# from debias.we import WordEmbedding
import numpy as np
import simplejson as json
import we
from we import WordEmbedding

from gensim.models.keyedvectors import KeyedVectors

#####################
# Globals/Hyperparameters #----------------------------------------------------
###########################
#Word vectors are saved in word2vec binary format
# Download the file from https://drive.google.com/file/d/0B7XkCwpI5KDYNlNUTTlSS21pQmM/edit
WORD_VECS_PATH = '../hate-tweet-data/debias_words/GoogleNews-vectors-negative300.bin'
W2V_MODEL = KeyedVectors.load_word2vec_format(WORD_VECS_PATH, binary=True) 


# Debiasing global parameters
E = WordEmbedding(WORD_VECS_PATH)

"""
----------------Debiasing methods------------------------------------------------
"""
def hard_debias(E_ori, gender_specific_words, definitional, equalize):
    global E

    gender_direction = we.doPCA(definitional, E_ori).components_[0]
    specific_set = set(gender_specific_words)
    for i, w in enumerate(E_ori.words):
        if w not in specific_set:
            E_ori.vecs[i] = we.drop(E_ori.vecs[i], gender_direction)
    E_ori.normalize()
    candidates = {x for e1, e2 in equalize for x in [(e1.lower(), e2.lower()),
                                                     (e1.title(), e2.title()),
                                                     (e1.upper(), e2.upper())]}
    print(candidates)
    for (a, b) in candidates:
        if (a in E_ori.index and b in E_ori.index):
            y = we.drop((E_ori.v(a) + E_ori.v(b)) / 2, gender_direction)
            z = np.sqrt(1 - np.linalg.norm(y)**2)
            if (E_ori.v(a) - E_ori.v(b)).dot(gender_direction) < 0:
                z = -z
            E_ori.vecs[E_ori.index[a]] = z * gender_direction + y
            E_ori.vecs[E_ori.index[b]] = -z * gender_direction + y
    E_ori.normalize()
    E = E_ori


if __name__ == '__main__':
    print("starting")
    # Load some gender related word lists for debiasing

    # get definitional pairs e.g (woman, man)
    with open('../hate-tweet-data/debias_words/gender/definitional_pairs.json', "r") as f:
        defs = json.load(f)
    print("definitional", defs)

    with open('../hate-tweet-data/debias_words/gender/equalize_pairs.json', "r") as f:
        equalize_pairs = json.load(f)
    print("equalize", equalize_pairs)

    # print the first 10/218 gender specific words e.g. acress, aunt, ballerina
    with open('../hate-tweet-data/debias_words/gender/gender_specific_seed.json', "r") as f:
        gender_specific_words = json.load(f)
    print("gender specific", len(gender_specific_words), gender_specific_words[:10])

    hard_debias(E, gender_specific_words, defs, equalize_pairs)

    E.save_w2v('../hate-tweet-data/debias_words/gender/GaoogleNews-vectors-debiased.bin')
