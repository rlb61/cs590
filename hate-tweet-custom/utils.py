#Rachel Draelos

#Credit: functions for translate() and clean() are based
#on https://github.com/sebastiandziadzio/hate-tweet

import re
import os
import string
import pandas as pd
import numpy as np

DATA_PATH = '/data/rlb61/privacy/data/jigsaw_original'

#############
# Functions #-------------------------------------------------------------------
#############
def prepare_data():
    #Read and merge
    train = pd.read_csv(os.path.join(DATA_PATH,'train.csv'),
                        header = 0, index_col = False)
    test_text = pd.read_csv(os.path.join(DATA_PATH,'test.csv'),
                        header = 0, index_col = False)
    test_labels = pd.read_csv(os.path.join(DATA_PATH,'test_labels.csv'),
                        header = 0, index_col = False)
    test = test_text.merge(test_labels, how = 'left', on = 'id')

    #Reformat and save
    clean_and_save(train, 'train')
    clean_and_save(test, 'test')
    
#helper
def clean_and_save(new_df, descriptor):
    """Reformat a dataframe <new_df> from
    ['id', 'toxic', 'severe_toxic', 'obscene', 'threat', 'insult',
       'identity_hate']
    to
    ['label','text']
    where text has been cleaned and labels are formatted as strings
    __label__toxic, __label__clean
    and **ASSUMPTION** __label__toxic is based on whether there is a
    POSITIVE ONE VALUE in ANY OF the original columns toxic, severe_toxic,
    obscene, threat, insult, and identity_hate.
    All zeros and negative ones are treated as zeros."""
    #First make the -1 equal to 0 (since it is not clear why there are -1
    #values in the labeling)
    new_df = new_df.replace(to_replace = -1, value = 0)
    
    #Aggregate labels
    new_df['label'] = 0
    new_df['label'] = new_df[['toxic','severe_toxic',
                              'obscene','threat','insult',
                              'identity_hate']].sum(axis = 1)
    new_df['label'] = (np.greater(new_df['label'].values, 0)).astype('int')
    
    #Rename 'comment_text' column to 'text'
    new_df = (new_df[['comment_text','label']]).rename(columns = {'comment_text':'text'})
    
    #Clean up the text
    new_df['text'] = new_df['text'].apply(clean)
    
    #Delete comments that are the empty string
    new_df = new_df[new_df['text']!='']
    
    #Save as csv
    new_df[['text','label']].to_csv(descriptor+'_clean.txt', index=False, header=True)

def ascii_only(tweet):
    """Remove non-ascii characters. You MUST decode again to Unicode otherwise
    you will not be able to look up the words in the word2vec model. If you
    encode to ascii without decoding to Unicode it will be a bytestring
    object instead of a regular Python string object."""
    return (tweet.encode('ascii', errors ='ignore')).decode('utf-8')

def test_data_cleaning_process():
    fake = pd.read_csv(os.path.join(DATA_PATH,'train_first_10.csv'),
                        header = 0, index_col = False)
    clean_and_save(fake,'delthis')
    clean_text_true = ["explanation why the edits made under my username hardcore metallica fan were reverted they werent vandalisms just closure on some gas after i voted at new york dolls fac and please dont remove the template from the talk page since im retired now892053827",
                       "daww he matches this background colour im seemingly stuck with thanks talk 2151 january 11 2016 utc",
                       "hey man im really not trying to edit war its just that this guy is constantly removing relevant information and talking to me through edits instead of my talk page he seems to care more about the formatting than the actual info",
                       "more i cant make any real suggestions on improvement i wondered if the section statistics should be later on or a subsection of types of accidents eg wikipediagoodarticlenominations"]
    clean_labels_true = [1,1,0,1]
    clean_text_produced = []
    clean_labels_produced = []
    with open('delthis_clean.txt','r') as f:
        next(f)
        for line in f:
            line = line.strip()
            tweet, label = line.split(',')
            clean_text_produced.append(tweet)
            clean_labels_produced.append(int(label))
    assert clean_text_true == clean_text_produced
    assert clean_labels_true == clean_labels_produced
    os.remove('delthis_clean.txt')
    print('Passed test_data_cleaning_process()')
    
####################################
# From sebastianziadzio/hate-tweet #--------------------------------------------
####################################
def translate(label):
    translation = {
        0: '__label__clean',
        1: '__label__toxic'}
    return translation[label]
    
def remove_hashtags(tweet):
    template = r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)" 
    tweet = re.sub(template, '', tweet)
    return tweet

def remove_mentions(tweet):
    template = r'(?:@[\w_]+)'
    tweet = re.sub(template, '', tweet)
    return tweet

def remove_punctuation(tweet):
    translation = str.maketrans('', '', string.punctuation)
    return tweet.translate(translation)

def remove_urls(tweet):
    template = r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+'
    tweet = re.sub(template, '', tweet)
    return tweet

def contract_whitespace(tweet):
    tweet = tweet.replace('\n', ' ')
    tweet = re.sub("\s\s+", " ", tweet.strip())
    return tweet 

def clean(tweet):
    tweet = remove_hashtags(tweet)
    tweet = remove_mentions(tweet)
    tweet = remove_urls(tweet)
    tweet = remove_punctuation(tweet)
    tweet = contract_whitespace(tweet)
    tweet = ascii_only(tweet)
    return tweet.lower()

if __name__=='__main__':
    test_data_cleaning_process()
    prepare_data()

